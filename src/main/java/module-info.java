module at.hakimst.javafxdaoexample {
    requires javafx.controls;
    requires javafx.fxml;
    requires java.sql;


    opens at.hakimst.javafxdaoexample to javafx.fxml;
    exports at.hakimst.javafxdaoexample;
    exports at.hakimst.javafxdaoexample.ui;
    opens at.hakimst.javafxdaoexample.ui to javafx.fxml;
}
