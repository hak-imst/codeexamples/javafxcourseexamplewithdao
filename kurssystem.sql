
CREATE TABLE `courses` (
  `id` int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT ,
  `name` varchar(64) NOT NULL,
  `description` varchar(256) NOT NULL,
  `hours` int(11) NOT NULL,
  `begindate` date NOT NULL,
  `enddate` date NOT NULL,
  `coursetype` varchar(64) NOT NULL
);

INSERT INTO `courses` (`name`, `description`, `hours`, `begindate`, `enddate`, `coursetype`) VALUES
('APR', 'Super Programmierkurs', 3, '2021-09-10', '2022-02-07', 'FF');

INSERT INTO `courses` (`name`, `description`, `hours`, `begindate`, `enddate`, `coursetype`) VALUES
    ('SWPP', 'Noch ein super Programmierkurs', 4, '2022-09-10', '2023-02-07', 'FF');

