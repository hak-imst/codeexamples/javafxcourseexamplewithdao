package at.hakimst.javafxdaoexample.ui;

import at.hakimst.javafxdaoexample.dataaccess.MyCourseRepository;
import at.hakimst.javafxdaoexample.dataaccess.MySqlCourseRepository;
import at.hakimst.javafxdaoexample.domain.Course;
import at.hakimst.javafxdaoexample.domain.CourseType;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.util.Callback;

import java.io.IOException;
import java.sql.Date;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.Optional;

public class CourseController {

    @FXML
    public TextField name;
    @FXML
    public TextField description;
    @FXML
    public TextField hours;
    @FXML
    public DatePicker begindate;
    @FXML
    public DatePicker enddate;
    @FXML
    public ComboBox coursetype;
    @FXML
    private TableView<Course> courseTableView;
    @FXML
    public TableColumn courseTableViewColumnName;
    @FXML
    public TableColumn courseTableViewColumnDescription;
    @FXML
    public TableColumn courseTableViewColumnHours;
    @FXML
    public TableColumn courseTableViewColumnBeginDate;
    @FXML
    public TableColumn courseTableViewColumnEndDate;
    @FXML
    public TableColumn courseTableViewColumnCourseType;


    private MyCourseRepository repository;

    public CourseController() throws SQLException, ClassNotFoundException {
        this.repository = new MySqlCourseRepository();
    }

    @FXML
    public void initialize() {

        name.setText("Mein neuer Kurs");

        description.setText("Meine Kursbeschreibung");

        hours.setText("2");

        LocalDate now = LocalDate.now();
        begindate.setValue(now);
        enddate.setValue(now.plusDays(14));

        ObservableList<CourseType> courseTypes =
                FXCollections.observableArrayList(
                        CourseType.FF,
                        CourseType.BF,
                        CourseType.ZA,
                        CourseType.OE
                );
        coursetype.setItems(courseTypes);
        coursetype.getSelectionModel().select(0);


        courseTableView.setPlaceholder(new Label("No rows to display"));

        courseTableViewColumnName.setCellValueFactory(c -> new SimpleStringProperty(((TableColumn.CellDataFeatures<Course,String>)c).getValue().getName()));
        courseTableViewColumnDescription.setCellValueFactory(c -> new SimpleStringProperty(((TableColumn.CellDataFeatures<Course,String>)c).getValue().getDescription()));
        courseTableViewColumnHours.setCellValueFactory(c -> new SimpleIntegerProperty(((TableColumn.CellDataFeatures<Course,String>)c).getValue().getHours()));
        courseTableViewColumnBeginDate.setCellValueFactory(c -> new SimpleStringProperty(((TableColumn.CellDataFeatures<Course,String>)c).getValue().getBeginDate().toString()));
        courseTableViewColumnEndDate.setCellValueFactory(c -> new SimpleStringProperty(((TableColumn.CellDataFeatures<Course,String>)c).getValue().getEndDate().toString()));
        courseTableViewColumnCourseType.setCellValueFactory(c -> new SimpleStringProperty(((TableColumn.CellDataFeatures<Course,String>)c).getValue().getCourseType().toString()));

        loadCourses();
    }


    private void loadCourses() {
        ObservableList<Course> data = FXCollections.observableArrayList (repository.getAll());
        courseTableView.setItems(data);
    }

    public void insertCourse(ActionEvent actionEvent) {
        String name = this.name.getText();
        String description = this.description.getText();
        int hours;
        try {
            hours = Integer.parseInt(this.hours.getText());
        } catch(NumberFormatException e) {
            System.out.println("Fehler beim Parsen von Stunden: " + e.getMessage());
            return;
        }

        Date beginDate = Date.valueOf(this.begindate.getValue());
        Date endDate = Date.valueOf(this.enddate.getValue());

        CourseType courseType = (CourseType) this.coursetype.getValue();

        Course course = new Course(name, description, hours, beginDate, endDate, courseType);

        System.out.println("insert course: " + course);
        Optional<Course> courseInserted = repository.insert(course);

        if (courseInserted.isPresent()) {
            System.out.println(courseInserted.get());
        } else {
            System.out.println("Failed");
        }

        loadCourses();
    }
}
