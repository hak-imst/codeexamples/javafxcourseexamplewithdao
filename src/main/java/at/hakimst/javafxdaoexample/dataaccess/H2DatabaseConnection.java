package at.hakimst.javafxdaoexample.dataaccess;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

public class H2DatabaseConnection {

    private static Connection con = null;

    private H2DatabaseConnection() {
    }

    public static Connection getConnection() throws ClassNotFoundException, SQLException {
        if (con != null) {
            return con;
        } else {
            Class.forName ("org.h2.Driver");
            con = DriverManager.getConnection("jdbc:h2:mem:kurssystem");

            // create table at startup
            String sql = "CREATE TABLE courses (" +
                    "  id int NOT NULL PRIMARY KEY AUTO_INCREMENT ," +
                    "  name varchar(64) NOT NULL," +
                    "  description varchar(256) NOT NULL," +
                    "  hours int NOT NULL," +
                    "  begindate date NOT NULL," +
                    "  enddate date NOT NULL," +
                    "  coursetype varchar(64) NOT NULL" +
                    ");";

            Statement statement = con.createStatement();
            statement.execute(sql);

            // insert demo data
            statement.execute("INSERT INTO courses (name, description, hours, begindate, enddate, coursetype) VALUES ('APR', 'Super Programmierkurs', 3, '2021-09-10', '2022-02-07', 'FF')");
            statement.execute("INSERT INTO courses (name, description, hours, begindate, enddate, coursetype) VALUES ('SWPP', 'Noch ein super Programmierkurs', 4, '2022-09-10', '2023-02-07', 'FF')");

            return con;
        }
    }
}
