package at.hakimst.javafxdaoexample.domain;

public enum CourseType {
    OE, BF, ZA, FF;
}
