package at.hakimst.javafxdaoexample.dataaccess;


import at.hakimst.javafxdaoexample.domain.Course;
import at.hakimst.javafxdaoexample.domain.CourseType;

import java.sql.Date;
import java.util.List;

public interface MyCourseRepository extends BaseRepository<Course, Long> {
    List<Course> findAllCoursesByName(String name);

    List<Course> findAllCoursesByDescription(String desription);

    List<Course> findAllCoursesByNameOrDescription(String searchText);

    List<Course> findAllCoursesByCourseType(CourseType courseType);

    List<Course> findAllCoursesByStartDate(Date startDate);

    List<Course> findAllRunningCourses();
}
